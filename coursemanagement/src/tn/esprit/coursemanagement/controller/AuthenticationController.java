package tn.esprit.coursemanagement.controller;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import tn.edu.esprit.arch.jee.coursemanagement.entities.Teacher;
import tn.edu.esprit.arch.jee.coursemanagement.exceptions.BadCredentialException;
import tn.edu.esprit.arch.jee.coursemanagement.services.interfaces.ITeacherDAOLocal;

@ManagedBean(name = "authenticationController")
@SessionScoped
public class AuthenticationController {

	@EJB
	ITeacherDAOLocal teacherEjb;

	private boolean loggedIn = false;
	private Teacher teacher = new Teacher();

	public String doLogIn() {
		try {
			teacher = teacherEjb.retrieveByCredentials(teacher.getPersonEmail(), teacher.getPersonPassword());
			loggedIn = true;
			return "teachers?faces-redirect=true";
		} catch (BadCredentialException ex) {
			FacesContext.getCurrentInstance().addMessage("formLogin:authFormMsg",new FacesMessage("Bad Credentials", "Please fill in good ones"));
			return null;
		}
	}

	public String doLogOut() {
		teacher = new Teacher();
		loggedIn = false;
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "login?faces-redirect=true";
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}
}
