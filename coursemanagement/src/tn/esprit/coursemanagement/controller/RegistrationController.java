package tn.esprit.coursemanagement.controller;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tn.edu.esprit.arch.jee.coursemanagement.entities.Teacher;
import tn.edu.esprit.arch.jee.coursemanagement.services.interfaces.ITeacherDAOLocal;

@ManagedBean(name="registrationController")
@ViewScoped
public class RegistrationController {
	
	@EJB
	ITeacherDAOLocal teacherEjb; 
	private Teacher teacher = new Teacher();
	
	public String doRegisterTeacher(){
		teacherEjb.create(teacher);
		return "login?faces-redirect=true";
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

}
