package tn.esprit.coursemanagement.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tn.edu.esprit.arch.jee.coursemanagement.entities.Teacher;
import tn.edu.esprit.arch.jee.coursemanagement.services.interfaces.ITeacherDAOLocal;

@ManagedBean(name = "teacherController")
@ViewScoped
public class TeacherController {

	@EJB
	ITeacherDAOLocal teacherEjb;
	private List<Teacher> teachers;
	private boolean display = false;
	private Teacher teacher = new Teacher();

	@PostConstruct
	private void init() {
		teachers = teacherEjb.retrieveAll();
	}

	public String doUpdateTeacher() {
		teacherEjb.update(teacher);
		display = false;
		return "teachers?faces-redirect=true";
	}

	public String doCancelUpdateTeacher() {
		return "teachers?faces-redirect=true";
	}

	public String doDeleteTeacher() {
		teacherEjb.delete(teacher);
		return "teachers?faces-context=true";
	}

	public List<Teacher> getTeachers() {
		return teachers;
	}

	public void setTeachers(List<Teacher> teachers) {
		this.teachers = teachers;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public boolean isDisplay() {
		return display;
	}

	public void setDisplay(boolean display) {
		this.display = display;
	}
}
