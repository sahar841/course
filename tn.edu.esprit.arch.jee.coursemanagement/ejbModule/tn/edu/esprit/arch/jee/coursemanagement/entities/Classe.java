package tn.edu.esprit.arch.jee.coursemanagement.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Classe implements Serializable {

	private int classeId;
	private String classeName;
	private String classeRoom;
	private List<Student> students;
	private List<Seance> seances;
	private static final long serialVersionUID = 1L;

	public Classe() {
		super();
	}

	public Classe(String classeName, String classeRoom) {
		super();
		this.classeName = classeName;
		this.classeRoom = classeRoom;
	}

	public Classe(String classeName, String classeRoom, List<Student> students) {
		super();
		this.classeName = classeName;
		this.classeRoom = classeRoom;
		this.students = students;
	}

	public Classe(String classeName, String classeRoom, List<Student> students,
			List<Seance> seances) {
		super();
		this.classeName = classeName;
		this.classeRoom = classeRoom;
		this.students = students;
		this.seances = seances;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getClasseId() {
		return this.classeId;
	}

	public void setClasseId(int classeId) {
		this.classeId = classeId;
	}

	public String getClasseName() {
		return this.classeName;
	}

	public void setClasseName(String classeName) {
		this.classeName = classeName;
	}

	public String getClasseRoom() {
		return this.classeRoom;
	}

	public void setClasseRoom(String classeRoom) {
		this.classeRoom = classeRoom;
	}

	@OneToMany(mappedBy = "classe",cascade=CascadeType.ALL)
	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	@OneToMany(mappedBy="classe")
	public List<Seance> getSeances() {
		return seances;
	}

	public void setSeances(List<Seance> seances) {
		this.seances = seances;
	}
}
