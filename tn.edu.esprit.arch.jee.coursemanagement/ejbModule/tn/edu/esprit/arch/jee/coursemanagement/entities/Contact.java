package tn.edu.esprit.arch.jee.coursemanagement.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class Contact implements Serializable {

	private int addressHouse;
	private String addressStreet;
	private String addressCity;
	private String addressCountry;
	private int phoneNumber;
	private String studentEmail;
	private int parentPhoneNumber;
	private String parentEmail;
	private static final long serialVersionUID = 1L;

	public Contact() {
		super();
	}

	public Contact(int addressHouse, String addressStreet, String addressCity,
			String addressCountry, int phoneNumber, String studentEmail,
			int parentPhoneNumber, String parentEmail) {
		super();
		this.addressHouse = addressHouse;
		this.addressStreet = addressStreet;
		this.addressCity = addressCity;
		this.addressCountry = addressCountry;
		this.phoneNumber = phoneNumber;
		this.studentEmail = studentEmail;
		this.parentPhoneNumber = parentPhoneNumber;
		this.parentEmail = parentEmail;
	}

	public int getAddressHouse() {
		return this.addressHouse;
	}

	public void setAddressHouse(int addressHouse) {
		this.addressHouse = addressHouse;
	}

	public String getAddressStreet() {
		return this.addressStreet;
	}

	public void setAddressStreet(String addressStreet) {
		this.addressStreet = addressStreet;
	}

	public String getAddressCity() {
		return this.addressCity;
	}

	public void setAddressCity(String addressCity) {
		this.addressCity = addressCity;
	}

	public String getAddressCountry() {
		return this.addressCountry;
	}

	public void setAddressCountry(String addressCountry) {
		this.addressCountry = addressCountry;
	}

	public int getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getStudentEmail() {
		return this.studentEmail;
	}

	public void setStudentEmail(String studentEmail) {
		this.studentEmail = studentEmail;
	}

	public int getParentPhoneNumber() {
		return this.parentPhoneNumber;
	}

	public void setParentPhoneNumber(int parentPhoneNumber) {
		this.parentPhoneNumber = parentPhoneNumber;
	}

	public String getParentEmail() {
		return this.parentEmail;
	}

	public void setParentEmail(String parentEmail) {
		this.parentEmail = parentEmail;
	}

}
