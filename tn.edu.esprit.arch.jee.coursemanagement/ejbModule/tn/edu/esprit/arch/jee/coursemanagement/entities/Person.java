package tn.edu.esprit.arch.jee.coursemanagement.entities;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class Person implements Serializable {

	private int personId;
	private String personFirstName;
	private String personLastName;
	private String personEmail;
	private String personPassword;
	private Calendar personBirthdate;
	private boolean personGender;
	private static final long serialVersionUID = 1L;

	public Person() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getPersonId() {
		return this.personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public String getPersonFirstName() {
		return this.personFirstName;
	}

	public void setPersonFirstName(String personFirstName) {
		this.personFirstName = personFirstName;
	}

	public String getPersonLastName() {
		return this.personLastName;
	}

	public void setPersonLastName(String personLastName) {
		this.personLastName = personLastName;
	}

	public Calendar getPersonBirthdate() {
		return this.personBirthdate;
	}

	public void setPersonBirthdate(Calendar personBirthdate) {
		this.personBirthdate = personBirthdate;
	}

	public boolean getPersonGender() {
		return this.personGender;
	}

	public void setPersonGender(boolean personGender) {
		this.personGender = personGender;
	}

	public String getPersonEmail() {
		return personEmail;
	}

	public void setPersonEmail(String personEmail) {
		this.personEmail = personEmail;
	}

	public String getPersonPassword() {
		return personPassword;
	}

	public void setPersonPassword(String personPassword) {
		this.personPassword = personPassword;
	}

}
