package tn.edu.esprit.arch.jee.coursemanagement.entities;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Seance implements Serializable {

	private SeancePK seancePK;
	private Teacher teacher;
	private Classe classe;
	private static final long serialVersionUID = 1L;

	public Seance() {
		super();
	}

	public Seance(SeancePK seancePK, Teacher teacher, Classe classe) {
		super();
		this.seancePK = seancePK;
		this.teacher = teacher;
		this.classe = classe;
	}

	@EmbeddedId
	public SeancePK getSeancePK() {
		return seancePK;
	}

	public void setSeancePK(SeancePK seancePK) {
		this.seancePK = seancePK;
	}

	@ManyToOne
	@JoinColumn(name = "teacherId", referencedColumnName = "personId", insertable = false, updatable = false)
	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	@ManyToOne
	@JoinColumn(name = "classeId", referencedColumnName = "classeId", insertable = false, updatable = false)
	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((seancePK == null) ? 0 : seancePK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Seance other = (Seance) obj;
		if (seancePK == null) {
			if (other.seancePK != null)
				return false;
		} else if (!seancePK.equals(other.seancePK))
			return false;
		return true;
	}
}
