package tn.edu.esprit.arch.jee.coursemanagement.entities;

import java.io.Serializable;

import javax.persistence.*;

@Embeddable
public class SeancePK implements Serializable {

	private int classeId;
	private int teacherId;
	private static final long serialVersionUID = 1L;

	public SeancePK() {
		super();
	}   
	
	public SeancePK(int classeId, int teacherId) {
		super();
		this.classeId = classeId;
		this.teacherId = teacherId;
	}

	public int getClasseId() {
		return this.classeId;
	}

	public void setClasseId(int classeId) {
		this.classeId = classeId;
	}   
	public int getTeacherId() {
		return this.teacherId;
	}

	public void setTeacherId(int teacherId) {
		this.teacherId = teacherId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + classeId;
		result = prime * result + teacherId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SeancePK other = (SeancePK) obj;
		if (classeId != other.classeId)
			return false;
		if (teacherId != other.teacherId)
			return false;
		return true;
	}   
}
