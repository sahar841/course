package tn.edu.esprit.arch.jee.coursemanagement.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name = "Student.findAllByName", query = "select s from Student s where s.personFirstName =:name")
public class Student extends Person implements Serializable {

	private Classe classe;
	private Contact contact;
	private static final long serialVersionUID = 1L;

	public Student() {
		super();
	}

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REMOVE })
	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	@Embedded
	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

}
