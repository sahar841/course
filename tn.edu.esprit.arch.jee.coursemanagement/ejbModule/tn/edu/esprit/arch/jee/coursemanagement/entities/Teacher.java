package tn.edu.esprit.arch.jee.coursemanagement.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQueries(value = {
		@NamedQuery(name = "Teacher.findAllByName", query = "select t from Teacher t where t.personFirstName =:name"),
		@NamedQuery(name = "Teacher.findByCredentials", query = "select t from Teacher t where t.personEmail =:email AND t.personPassword =:password") })
public class Teacher extends Person implements Serializable {

	private List<Seance> seances;
	private static final long serialVersionUID = 1L;

	public Teacher() {
		super();
	}

	@OneToMany(mappedBy = "teacher")
	public List<Seance> getSeances() {
		return seances;
	}

	public void setSeances(List<Seance> seances) {
		this.seances = seances;
	}
}
