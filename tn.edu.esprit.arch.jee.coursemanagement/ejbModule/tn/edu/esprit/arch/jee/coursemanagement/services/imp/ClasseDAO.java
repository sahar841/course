package tn.edu.esprit.arch.jee.coursemanagement.services.imp;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.edu.esprit.arch.jee.coursemanagement.entities.Classe;
import tn.edu.esprit.arch.jee.coursemanagement.services.interfaces.IClasseDAOLocal;
import tn.edu.esprit.arch.jee.coursemanagement.services.interfaces.IClasseDAORemote;

@Stateless
public class ClasseDAO implements IClasseDAORemote, IClasseDAOLocal {

	@PersistenceContext
	EntityManager em;

	public ClasseDAO() {
	}

	@Override
	public void create(Classe classe) {
		em.persist(classe);
	}

	@Override
	public Classe retrieve(int id) {
		return em.find(Classe.class, id);
	}

	@Override
	public void update(Classe classe) {
		em.merge(classe);
	}

	@Override
	public void delete(Classe classe) {
		em.remove(classe);
	}

	@Override
	public List<Classe> retrieveAll() {
		TypedQuery<Classe> query = em.createQuery("select c from Classe c",
				Classe.class);
		return query.getResultList();
	}

	@Override
	public List<Classe> retrieveAllByName(String name) {
		TypedQuery<Classe> query = em.createNamedQuery("Classe.findAllByName", Classe.class);
		query.setParameter("name", name);
		return query.getResultList();
	}

}
