package tn.edu.esprit.arch.jee.coursemanagement.services.imp;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.edu.esprit.arch.jee.coursemanagement.entities.Student;
import tn.edu.esprit.arch.jee.coursemanagement.services.interfaces.IStudentDAOLocal;
import tn.edu.esprit.arch.jee.coursemanagement.services.interfaces.IStudentDAORemote;

@Stateless
public class StudentDAO implements IStudentDAORemote, IStudentDAOLocal {

	@PersistenceContext
	EntityManager em;

	public StudentDAO() {
	}

	@Override
	public void create(Student student) {
		em.persist(student);
	}

	@Override
	public Student retrieve(int id) {
		return em.find(Student.class, id);
	}

	@Override
	public void update(Student student) {
		em.merge(student);
	}

	@Override
	public void delete(Student student) {
		em.remove(student);
	}

	@Override
	public List<Student> retrieveAll() {
		TypedQuery<Student> query = em.createQuery("select s from Student s",
				Student.class);
		return query.getResultList();
	}

	@Override
	public List<Student> retrieveAllByName(String name) {
		TypedQuery<Student> query = em.createNamedQuery(
				"Student.findAllByName", Student.class);
		query.setParameter("name", name);
		return query.getResultList();
	}

}
