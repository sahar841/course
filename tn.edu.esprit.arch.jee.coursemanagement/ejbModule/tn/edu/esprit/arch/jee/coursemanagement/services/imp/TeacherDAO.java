package tn.edu.esprit.arch.jee.coursemanagement.services.imp;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.edu.esprit.arch.jee.coursemanagement.entities.Teacher;
import tn.edu.esprit.arch.jee.coursemanagement.exceptions.BadCredentialException;
import tn.edu.esprit.arch.jee.coursemanagement.services.interfaces.ITeacherDAOLocal;
import tn.edu.esprit.arch.jee.coursemanagement.services.interfaces.ITeacherDAORemote;

@Stateless
public class TeacherDAO implements ITeacherDAORemote, ITeacherDAOLocal {

	@PersistenceContext
	EntityManager em;

	public TeacherDAO() {
	}

	@Override
	public void create(Teacher teacher) {
		em.persist(teacher);
	}

	@Override
	public Teacher retrieve(int id) {
		return em.find(Teacher.class, id);
	}

	@Override
	public void update(Teacher teacher) {
		em.merge(teacher);
	}

	@Override
	public void delete(Teacher teacher) {
		em.remove(em.find(Teacher.class, teacher.getPersonId()));
	}

	@Override
	public List<Teacher> retrieveAll() {
		TypedQuery<Teacher> query = em.createQuery("select t from Teacher t",
				Teacher.class);
		return query.getResultList();
	}

	@Override
	public List<Teacher> retrieveAllByName(String name) {
		TypedQuery<Teacher> query = em.createNamedQuery(
				"Teacher.findAllByName", Teacher.class);
		query.setParameter("name", name);
		return query.getResultList();
	}
	
	@Override
	public Teacher retrieveByCredentials(String username, String password) throws BadCredentialException {
		TypedQuery<Teacher> query = em.createNamedQuery(
				"Teacher.findByCredentials", Teacher.class);
		query.setParameter("email", username);
		query.setParameter("password", password);
		try {
			return query.getSingleResult();
		} catch (NoResultException ex) {
			throw new BadCredentialException();
		}

	}

}
