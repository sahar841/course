package tn.edu.esprit.arch.jee.coursemanagement.services.interfaces;

import java.util.List;

import javax.ejb.Remote;

import tn.edu.esprit.arch.jee.coursemanagement.entities.Classe;

@Remote
public interface IClasseDAORemote {

	public void create(Classe classe);

	public Classe retrieve(int id);

	public void update(Classe classe);

	public void delete(Classe classe);

	public List<Classe> retrieveAll();

	public List<Classe> retrieveAllByName(String name);
}
