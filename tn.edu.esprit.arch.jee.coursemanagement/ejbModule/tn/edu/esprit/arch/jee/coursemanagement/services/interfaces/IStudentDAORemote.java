package tn.edu.esprit.arch.jee.coursemanagement.services.interfaces;

import java.util.List;

import javax.ejb.Remote;

import tn.edu.esprit.arch.jee.coursemanagement.entities.Student;

@Remote
public interface IStudentDAORemote {

	public void create(Student student);

	public Student retrieve(int id);

	public void update(Student student);

	public void delete(Student student);

	public List<Student> retrieveAll();

	public List<Student> retrieveAllByName(String name);
}
