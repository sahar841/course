package tn.edu.esprit.arch.jee.coursemanagement.services.interfaces;

import java.util.List;

import javax.ejb.Local;

import tn.edu.esprit.arch.jee.coursemanagement.entities.Teacher;
import tn.edu.esprit.arch.jee.coursemanagement.exceptions.BadCredentialException;

@Local
public interface ITeacherDAOLocal {

	public void create(Teacher teacher);

	public Teacher retrieve(int id);

	public void update(Teacher teacher);

	public void delete(Teacher teacher);

	public List<Teacher> retrieveAll();

	public List<Teacher> retrieveAllByName(String name);
	
	public Teacher retrieveByCredentials(String username, String password) throws BadCredentialException;
}
